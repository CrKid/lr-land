<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Boretscy A">
    <title>Автомобили Land Rover — Юг-Авто</title>
	
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.2/flatpickr.min.css" />
    
    <link href="assets/fonts/fonts.css?<?=md5_file(__DIR__.'/assets/fonts/fonts.css')?>" rel="stylesheet" />
	<link href="assets/css/app.css?<?=md5_file(__DIR__.'/assets/css/app.css')?>" rel="stylesheet" />

  </head>
  <body>
    
    <?php include __DIR__.'/include/_counters.php';?>
    
    <div class="container">
    	
      <div class="row py-3 top-row">
        <div class="col-8 col-sm-7 col-lg-5">
          <a href="/">
            <img src="assets/images/logo.png" />
            <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-LogoYugavto"></use></svg>
          </a>
        </div>
        <div class="header-phone col-sm-3 d-none d-sm-inline pt-3 pr-3">
          <a href="tel:+78612100044">+7 (861) 210-00-44</a>
        </div>
        <div class="header-button text-center col-2 col-sm-1 col-lg-2 pt-3 pc">
            <a href="#callback">Заказать звонок <i class="icon-phone"></i></a>
        </div>
        <div class="header-button text-center col-2 col-sm-1 col-lg-2 pt-3 pc">
            <a href="#question">Задать вопрос <i class="icon-comment"></i></a>
        </div>
        <div class="header-button text-center col-2 col-sm-1 col-lg-2 pt-2 mob">
            <a href="tel:+78612100044"><i class="icon-phone"></i></a>
        </div>
        <div class="header-button text-center col-2 col-sm-1 col-lg-2 py-2 mob">
            <a href="#question"><i class="icon-comment"></i></a>
        </div>
      </div>
      
      <div class="row position-relative">
      	<div class="col-md-12 p-0">
          <img class="w-100 pc" src="assets/images/12-2019-3/1260x723.jpg" />
          <img class="w-100 mob" src="assets/images/12-2019-3/768x437.jpg" />
        </div>
        <div class="col position-absolute plate p-5">
          <h1 class="mb-3">21.12 Закрытая новогодняя Елка!</h1><!-- RANGE ROVER SPORT ОТ 4 817 000 РУБЛЕЙ -->
          <h5 class="mb-3 pb-3">Неприлично смелые предложения!</h5> <!-- Главный шанс года, когда автомобиль вашей мечты становится фантастически доступным. -->
          <a href="#offer" role="Offer" class="but but-black py-2 px-4 my-5"><i class="icon-email"></i> Узнать больше</a>
        </div>
      </div>
      
      <div class="row text-center my-5">
        <h2 class="py-2 bordered mx-auto">Выберите свой Land Rover</h2>
      </div>
      
      <div class="row text-center cars">
        
        <div class="col-sm-6 col-md-4 p-0 pb-5">
          <img class="img-fluid" src="assets/images/cars/Range_Rover_670x385.jpg" />
          <h3 class="d-inline-block bordered py-2 my-4">Range Rover</h3>
          <h5 class="c-gray mb-3 pb-3 px-3">Вершина эволюции</h5>
          <p class="mb-2 pb-3">&nbsp;</p>
          <a href="#offer" role="Offer" class="but but-black py-2 px-4 my-5" data-car="Range Rover"><i class="icon-offers-finance"></i> Получить выгоду от директора</a>
          <p class="my-2"><small><a href="#disclRangeRover"><sup>*</sup> Подробные условия действия предложения</a></small></p>
        </div>
        <div class="col-sm-6 col-md-4 p-0 pb-5">
          <img class="img-fluid" src="assets/images/cars/Range_Rover_Sport_670x385.jpg" />
          <h3 class="d-inline-block bordered py-2 my-4">Range Rover Sport</h3>
          <h5 class="c-gray mb-3 pb-3 px-3">Самый динамичный из Range Rover</h5>
          <p class="mb-2 pb-3">Преимущество до 742 000 ₽<sup>*</sup></p>
          <a href="#offer" role="Offer" class="but but-black py-2 px-4 my-5" data-car="Range Rover Sport"><i class="icon-offers-finance"></i> Получить предложение</a>
          <p class="my-2"><small><a href="#disclRangeRoverSport"><sup>*</sup> Подробные условия действия предложения</a></small></p>
        </div>
        <div class="col-sm-6 col-md-4 p-0 pb-5">
          <img class="img-fluid" src="assets/images/cars/L560_18MY_237_GLHD_DX_V2_Device-Desktop_670x385_308-355806_670x385.jpg" />
          <h3 class="d-inline-block bordered py-2 my-4">Range Rover Velar</h3>
          <h5 class="c-gray mb-3 pb-3 px-3">RANGE ROVER С АВАНГАРДНЫМ СТИЛЕМ</h5>
          <p class="mb-2 pb-3">Преимущество до 632 000 ₽<sup>*</sup></p>
          <a href="#offer" role="Offer" class="but but-black py-2 px-4 my-5" data-car="Range Rover Velar"><i class="icon-offers-finance"></i> Получить выгоду от директора</a>
          <p class="my-2"><small><a href="#disclRangeRoverVelar"><sup>*</sup> Подробные условия действия предложения</a></small></p>
        </div>

        <div class="col-sm-6 col-md-4 p-0 pb-5">
          <img class="img-fluid" src="assets/images/cars/Range_Rover_Evoque_670x385.jpg" />
          <h3 class="d-inline-block bordered py-2 my-4">Range Rover Evoque</h3>
          <h5 class="c-gray mb-3 pb-3 px-3">ФОРМА ПРЕВОСХОДСТВА</h5>
          <p class="mb-2 pb-3">от 2 601 000 ₽<sup>*</sup></p>
          <a href="#offer" role="Offer" class="but but-black py-2 px-4 my-5" data-car="Range Rover Evoque"><i class="icon-offers-finance"></i> Получить выгоду от директора</a>
          <p class="my-2"><small><a href="#disclRangeRoverEvoque"><sup>*</sup> Подробные условия действия предложения</a></small></p>
        </div>
        <div class="col-sm-6 col-md-4 p-0 pb-5">
          <img class="img-fluid" src="assets/images/cars/670x385.jpg" />
          <h3 class="d-inline-block bordered py-2 my-4">Discovery</h3>
          <h5 class="c-gray mb-3 pb-3 px-3">Непревзойденные возможности</h5>
          <p class="mb-2 pb-3">Преимущество до 638 000 ₽<sup>*</sup></p>
          <a href="#offer" role="Offer" class="but but-black py-2 px-4 my-5" data-car="Range Rover Sport"><i class="icon-offers-finance"></i> Получить предложение</a>
          <p class="my-2"><small><a href="#disclDiscovery"><sup>*</sup> Подробные условия действия предложения</a></small></p>
        </div>
        <div class="col-sm-6 col-md-4 p-0 pb-5">
          <img class="img-fluid" src="assets/images/cars/NEW-Discovery_Sport_670x385.jpg" />
          <h3 class="d-inline-block bordered py-2 my-4">Новый Discovery Sport</h3>
          <h5 class="c-gray mb-3 pb-3 px-3">Универсальный внедорожник</h5>
          <p class="mb-2 pb-3">от 2 730 000 ₽<sup>*</sup></p>
          <a href="#offer" role="Offer" class="but but-black py-2 px-4 my-5" data-car="Новый Discovery Sport"><i class="icon-offers-finance"></i> Получить выгоду от директора</a>
          <p class="my-2"><small><a href="#disclDiscoverySport"><sup>*</sup> Подробные условия действия предложения</a></small></p>
        </div>

      </div>
      
      <div class="row trade-in">
        <div class="col-md-4 bg-gray2">
          <div class="d-inline-block title">Больше преимуществ</div>
          <h2>Trade in<br />Меняйтесь к лучшему</h2>
          <p>Вы можете приобрести новый Land Rover, сдав свой старый автомобиль в зачет стоимости нового. Стоимость вашего автомобиля зачитывается в стоимость покупки, а дополнительное преимущество при сдаче Вашего автомобиля в Trade-In может составить до 250 000 ₽ от стоимости нового автомобиля. Кроме того, при покупке любой модели Land Rover вы можете воспользоваться широким выбором кредитных программ и предложений.</p>
          <div class="w-100" style="clear:both;"></div>
          <a href="#tradein" class="but but-black py-2 px-4 ml-5 my-5"><i class="icon-request-quote"></i> Рассчитать стоимость</a>
          <div class="w-100 py-3" style="clear:both;"></div>
        </div>
      </div>
      
      <div class="row bg-gray4 tfut py-5 px-4">
        <div class="col-md-12 text-center"><h2 class="d-inline-block py-2 mt-4 bordered mx-auto">Обмен с преимуществом</h2></div>
        <div class="col-md-3 mt-4 px-4">
          <div class="title pb-4">01 <span class="float-right"><i class="icon-ignite-configure"></i></span></div>
          <div class="subtitle py-2">Приезжаете к нам и выбираете новый Land Rover</div>
          <p class="my-4">Позвоните в отдел продаж и договоритесь о визите в удобное для Вас время или приезжайте в рабочие часы без звонка.</p>
        </div>
        <div class="col-md-3 mt-4 px-4">
          <div class="title pb-4">02 <span class="float-right"><i class="icon-request-quote"></i></span></div>
          <div class="subtitle py-2">Проводим оценку и диагностику вашего автомобиля</div>
          <p class="my-4">Предоставьте свой автомобиль для оценки предварительной стоимости. Если сумма устраивает, проходите техническую диагностику.</p>
        </div>
        <div class="col-md-3 mt-4 px-4">
          <div class="title pb-4">03 <span class="float-right"><i class="icon-social-facebook-share"></i></span></div>
          <div class="subtitle py-2">Определяете сумму зачета и заключаете договор</div>
          <p class="my-4">После экспертного осмотра и полной диагностики определяется конечная сумма зачета и заключается договор с учетом ценовой разницы</p>
        </div>
        <div class="col-md-3 mt-4 px-4">
          <div class="title pb-4">04 <span class="float-right"><i class="icon-calculate-trade-in-value"></i></span></div>
          <div class="subtitle py-2">Уезжаете на вашем новом автомобиле Land Rover</div>
          <p class="my-4">Сдаете ваш старый автомобиль и уезжаете на новом Land Rover. Не упустите главный шанс этого сезона!</p>
        </div>
        <div class="col-md-12 my-5 text-center"><a href="#tradein" class="but but-white py-2 px-4 my-5"><i class="icon-compare"></i> Заявка на обмен</a></div>
      </div>
      
      <div class="row bg-gray3 test p-5">
        <div class="col-md-12 mb-5 text-center"><h2 class="d-inline-block py-2 mt-4 bordered mx-auto">Запишитесь на тест-драйв</h2></div>
        <div class="col-md-6 px-4"><h3 style="line-height: 1.3">Выберите любое удобное для вас время и место для пробной поездки!</h3></div>
        <div class="col-md-6 px-4">
          <p>Откройте для себя премиальные и технологичные автомобили Land Rover. В дилерском центре Юг-Авто вы можете выбрать и купить любую модель Land Rover, включая знаменитую линейку Range Rover, получить профессиональную консультацию, оформить кредит, страховку, лизинг.</p>
          <p>Не упустите возможность стать владельцем Land Rover на уникальных условиях!</p>
        </div>
        <form class="w-100" data-event="test-drive">
          <input type="hidden" name="Form" value="Запишитесь на тест-драйв" />
          <div class="row">
            <div class="col-md-6 px-4 my-4">
              <div class="form-group">
                <label class="col-form-label">Ваше имя: <sup>*</sup></label>
                <input type="text" class="form-control" name="Name" required />
              </div>
              <div class="form-group">
                <label class="col-form-label">Контактный телефон: <sup>*</sup></label>
                <input type="phone" class="form-control" name="Phone" required />
              </div>
            </div>
            <div class="col-md-6 px-4 my-4">
              <div class="form-group">
                <label class="col-form-label">Выберите автомобиль: <sup>*</sup></label>
                <select class="form-control" name="Car" required>
                  <option disabled="" selected="">Автомобиль</option>
                  <option value="Range Rover">Range Rover</option>
                  <option value="Range Rover Sport">Range Rover Sport</option>
                  <option value="Range Rover Velar">Range Rover Velar</option>
                  <option value="Range Rover Evoque">Range Rover Evoque</option>
                  <option value="Discovery">Discovery</option>
                  <option value="New Discovery Sport">Новый Discovery Sport</option>
                </select>
              </div>
              <div class="form-group">
                <label class="col-form-label">Желаемая дата:</label>
                <input type="date" class="form-control" name="Date" />
              </div>
            </div>
            <div class="col-md-12 text-center px-4 my-4">
              <div><a href="#" role="Send" class="but but-black py-2 px-4 my-5"><i class="icon-ignite-drive"></i> Записаться на тест-драйв</a></div>
              <div class="w-100" style="clear:both;"></div>
              <p class="pt-4"><small>Поля, отмеченные *, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на получение от Jaguar Land Rover новостей, а также информации по специальным предложениям и рекламным акциям.</small></p>
            </div>
          </div>
        </form>
        <div class="alert alert-dismissible alert-success w-100">
          <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
        </div>
        <div class="alert alert-dismissible alert-danger w-100">
          <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
        </div>
      </div>
      
      <div class="row position-relative">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa5922d70942d39e0db0f998fd594f5ba6f39a6ac54fd06ff2eb83f2eb53fda47&amp;width=100%25&amp;height=450&amp;lang=ru_RU&amp;scroll=true"></script>
        <div class="col-md-4 contacts p-4">
          <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-LogoYugavto"></use></svg>
          <h5 class="mt-4">Россия, а.Тахтамукай, ул. Краснодарская, д.3</h5>
          <h5><i class="icon-phone"></i> <a href="tel:+78612100044">+7 (861) 210-00-44</a></h5>
          <p class="my-4">Режим работы: ежедневно 8:00-20:00</p>
          <a href="#callback" class="but but-black py-2 px-4 my-5"><i class="icon-phone"></i> Обратный звонок</a>
        </div>
      </div>
      
      <div class="row footer my-5">
        <div class="col-md-8">
          <p><small>&copy; ООО «Ягуар Ленд Ровер» <?=date('Y')?><br />&copy; Юг-Авто <?=date('Y')?></small></p>
        </div>
        <div class="col-md-2">
          <a href="https://www.facebook.com/landrover.yugavto/" target="_blank" class="bg-gray1 hbg-gray2 p-3"><i class="icon-social_facebook"></i></a> 
          <a href="https://vk.com/landrover.yugavto" target="_blank" class="bg-gray1 hbg-gray2 p-3"><i class="icon-social_vkontakt"></i></a>
          <a href="https://www.instagram.com/jlr_yugavto/" target="_blank" class="bg-gray1 hbg-gray2 p-3"><i class="icon-social_instagram"></i></a> 
        </div>
      </div>
    
    </div>
    
    <?php include __DIR__.'/include/_modals.php';?>
    <?php include __DIR__.'/include/_svg.php';?>
    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.2/flatpickr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.2/l10n/ru.js"></script>
    
    <script src="assets/js/app.js"></script>
    
    <?php include __DIR__.'/include/_scripts.php';?>
    
  </body>
</html>
