'use strict';

$('input[type="phone"]').inputmask({ 'mask': '+7 (999) 999-99-99', showMaskOnHover: false });
$('input[type="date"]').flatpickr({
    locale: "ru",
    altInput: true
});


$('a[role="Offer"]').click(function() {

    var Car = (typeof $(this).data('car') != 'undefined') ? $(this).data('car') : '';

    var w = $('div[data-remodal-id="offer"]');
    w.find('input[name="Car"]').val(Car);
    w.find('h2[role="Car"]').text(Car);

});


$('a[role="Send"]').click(function() {

    var JSApp = {};
    JSApp.SendData = {};

    var Form = $(this).parent().parent().parent().parent();
    var success = $(Form).parent().find('.alert-success');
    var error = $(Form).parent().find('.alert-danger');
	var Event = $(Form).data('event');

    JSApp.SendData.AppName = 'Lands';
    JSApp.SendData.EventCategory = 'Заполена форма';

    JSApp.SendData.Flag = true;

    Form.find('input, select, textarea').each(function(i, e) {

        JSApp.SendData[$(e).attr('name')] = $(e).val();
        $(e).removeClass('is-invalid');

        if ($(e).attr('required')) {

            if (!$(e).val()) {

                JSApp.SendData.Flag = false;
                $(e).addClass('is-invalid');
            }
        }
    });
	
    if (JSApp.SendData.Flag) {

        $.ajax({
            type: "POST",
            url: '/ajax/',
            data: JSApp.SendData,
            success: function(data) {

                var res = JSON.parse(data);

                if (res.status == 'success') {

                    $(Form).slideUp(300);
                    $(success).slideDown(300);
                    $(error).hide();
					
					if (typeof yaCounter48807995 != 'undefined') yaCounter48807995.reachGoal( 'LRLandind_send');
                    if (typeof Matomo != 'undefined') _paq.push(["trackEvent", 'Формы лэндинга', YApps.SendData.Form, 'Отправка']);
					JSApp.Layer = {
						'event': 'FormSubmission',
						'eventCategory': Event,
						'eventAction': 'submit',
						'eventLabel': JSApp.SendData.Car || null
					};
					dataLayer.push( JSApp.Layer );

                    
                    var CallTouchURL = 'https://api-node7.calltouch.ru/calls-service/RestAPI/requests/28255/register/';
                    CallTouchURL += '?subject=Формы лэндинга - '+JSApp.SendData.Form;
                    CallTouchURL += '&sessionId='+window.call_value_37;
                    CallTouchURL += '&fio='+JSApp.SendData.Name;
                    CallTouchURL += '&phoneNumber='+JSApp.SendData.Phone.replace(/[^\d;]/g, '');
					
                    $.get( CallTouchURL );
					
					YApps.AppPushStat( JSApp.SendData );

                } else {

                    $(error).slideDown(300);
                }
            },
            error: function() {

                $(error).slideDown(300);
            }
        });
		
    }

    return false;
});