﻿<!doctype html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Boretscy A">
    <title>Автомобили Land Rover — Юг-Авто</title>
	
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/icon/favicon-16x16.png">
    <link rel="manifest" href="assets/images/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/images/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    
    <link href="assets/fonts/fonts.css?<?=md5_file(__DIR__.'/assets/fonts/fonts.css')?>" rel="stylesheet" />
	<link href="assets/css/app.css?<?=md5_file(__DIR__.'/assets/css/app.css')?>" rel="stylesheet" />


  </head>
  <body>
    
    <div class="container">
    	
      <div class="row">
      	
        <?php $str = 'icon-360-evo-jaguar.icon-360-view.icon-accessories-evo-jaguar.icon-accessories.icon-accolade-chevron-left.icon-accolade-chevron-right.icon-add-evo-jaguar.icon-approved-used.icon-approvedused-evo-jaguar.icon-apps-evo-jaguar.icon-arduous-evo-jaguar.icon-arrow-down.icon-back-to-top.icon-bars.icon-battery-evo-jaguar.icon-bodyshop-evo-jaguar.icon-bodyshop.icon-bond.icon-brakefluid-evo-jaguar.icon-brakepads-evo-jaguar.icon-brochure-evo-jaguar.icon-calculate-trade-in-value.icon-calendar-evo-jaguar.icon-calendar.icon-call-evo-jaguar.icon-capacity-evo-jaguar.icon-captions-on.icon-captions.icon-caution-evo-jaguar.icon-caution.icon-chevron-down.icon-chevron-up.icon-chevronleft-evo-jaguar.icon-chevronright-evo-jaguar.icon-climate-jaguar.icon-clock-evo-jaguar.icon-close-evo-jaguar.icon-close.icon-close_v2.icon-close_v3.icon-co2-evo-jaguar.icon-comment.icon-compare-evo-jaguar.icon-compare-jaguar.icon-compare.icon-comparison-jaguar.icon-contact-evo-jaguar.icon-coolant-evo-jaguar.icon-credit-card-jaguar.icon-credit-card.icon-directional-left.icon-directional-right.icon-directions-jaguar.icon-directions.icon-dj-grass.icon-dj-gravel-rocks.icon-dj-mud-ruts.icon-dj-on-road.icon-dj-sand.icon-dj-snow-ice.icon-dj-water.icon-dots-evo-jaguar.icon-download-evo-jaguar.icon-download.icon-drag-arrow-down.icon-drag-arrow-left.icon-drag-arrow-right.icon-drag-arrow-up.icon-drive-evo-jaguar.icon-driveoffroad.icon-driveonroad-jaguar.icon-driveonroad.icon-edit-evo-jaguar.icon-email-evo-jaguar.icon-email.icon-exclamation-lr.icon-experience-evo-jaguar.icon-experience.icon-explore.icon-favourite-evo-jaguar.icon-fax-evo-jaguar.icon-fax.icon-filter-evo-jaguar.icon-filter.icon-filterclose-evo-jaguar.icon-finance-calculator-jaguar.icon-finance-calculator.icon-financecalculator-evo-jaguar.icon-fuel-evo-jaguar.icon-fullscreen-evo-jaguar.icon-fullscreen-exit.icon-fullscreen.icon-gallery-categories.icon-gallery-evo-jaguar.icon-guides-evo-jaguar.icon-help-evo-jaguar.icon-ignite-brochure-jaguar.icon-ignite-brochure.icon-ignite-configure-jaguar.icon-ignite-configure.icon-ignite-drive.icon-ignite-inform-jaguar.icon-ignite-inform.icon-incontrol-evo-jaguar.icon-info-evo-jaguar.icon-information-landrover.icon-information.icon-inventory-evo-jaguar.icon-inventory.icon-jag2you-evo-jaguar.icon-land-rover-to-you.icon-left-open-big.icon-link-evo-jaguar.icon-location-evo-jaguar.icon-location.icon-login-evo-jaguar.icon-map-pin-jaguar.icon-map-pin.icon-media-jaguar.icon-minus-jaguar.icon-minus.icon-motorway-jaguar.icon-new-star-jaguar.icon-new-star.icon-nocostoption.icon-notes-evo-jaguar.icon-notes.icon-notifications-evo-jaguar.icon-offereuro-evo-jaguar.icon-offerpound-evo-jaguar.icon-offers-finance-jaguar.icon-offers-finance.icon-oil-evo-jaguar.icon-optional.icon-ordertracker-evo-jaguar.icon-owners.icon-parts-evo-jaguar.icon-parts.icon-pause-evo-jaguar.icon-pause.icon-phone-jaguar.icon-phone.icon-play-evo-jaguar.icon-play.icon-plus-jaguar.icon-plus.icon-predelivery-evo-jaguar.icon-preparationsale-evo-jaguar.icon-print-evo-jaguar.icon-print.icon-profile-evo-jaguar.icon-profile.icon-question-mark.icon-quickstartguides-evo-jaguar.icon-quote-left-mena.icon-quote-left.icon-quote-right-mena.icon-quote-right.icon-quotegbp-evo-jaguar.icon-quoteusd-evo-jaguar.icon-reload-evo-jaguar.icon-reminder-evo-jaguar.icon-remove-evo-jaguar.icon-repair.icon-replay.icon-request-quote-dollar-jaguar.icon-request-quote-uk-jaguar.icon-request-quote.icon-right-open-big.icon-salespreferred-evo-jaguar.icon-save-evo-jaguar.icon-search-evo-jaguar.icon-search.icon-seats-evo-jaguar.icon-service-evo-jaguar.icon-service.icon-servicehistory-evo-jaguar.icon-servicepreferred-evo-jaguar.icon-share-evo-jaguar.icon-share-jaguar.icon-share.icon-shop-evo-jaguar.icon-shop-jaguar.icon-shop.icon-slide-out-icon.icon-snowflake-jaguar.icon-social-facebook-comment.icon-social-facebook-like.icon-social-facebook-share.icon-social-instagram-comment.icon-social-instagram-like.icon-social-pinterest-like.icon-social-pinterest-pin.icon-social-twitter-favourite.icon-social-twitter-retweet.icon-social_blogger.icon-social_douban.icon-social_facebook".icon-social_google.icon-social_instagram.icon-social_jaguarapp.icon-social_linkedin.icon-social_lrapp.icon-social_pinterest.icon-social_renren.icon-social_sinaweibo.icon-social_tumblr.icon-social_twitter.icon-social_vkontakt.icon-social_wechat.icon-social_whatsapp.icon-social_youtube.icon-sound-evo-jaguar.icon-sound-off.icon-sound-on.icon-speed-evo-jaguar.icon-speed-jaguar.icon-standard.icon-stepone-evo-jaguar.icon-stepthree-evo-jaguar.icon-steptwo-evo-jaguar.icon-subtitles-on.icon-subtitles.icon-svo.icon-target-evo-jaguar.icon-thumbnail_view.icon-tick-lr.icon-tick.icon-tickcircle-evo-jaguar.icon-tyrepressure-evo-jaguar.icon-value-jaguar.icon-vehicle-evo-jaguar.icon-vehicleadd-evo-jaguar.icon-videoguides-evo-jaguar.icon-view-vehicles.icon-warning-evo-jaguar.icon-warning.icon-washerfluid-evo-jaguar.icon-xcircle-evo-jaguar.icon-zoom-evo-jaguar.icon-zoom.icon-zoom_in.icon-zoom_out';
		
		$ar = explode('.', $str);
		
		?>
        <div class="col-md-12 mb-5" style="font-size: 2em;">
		<?php foreach ( $ar as $a ) { ?>
        <i class="<?=$a?>"></i>
        <? } ?>
		</div>
		<?php foreach ( $ar as $a ) { ?>
      	
        <div class="col-md-3"><?=$a?>: <i class="<?=$a?>"></i></div>
        
        <? } ?>
        
        
      </div>
    
    </div>
    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
    
    <script src="assets/js/app.js"></script>
    
  </body>
</html>
