<div class="remodal bg-gray2" data-remodal-id="callback">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Заказать звонок</h3></div>
  </div>
  <form data-event="callback">
    <input type="hidden" name="Form" value="Заказать звонок" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <label class="col-form-label">Ваше имя: <sup>*</sup></label>
          <input type="text" class="form-control" name="Name" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Контактный телефон: <sup>*</sup></label>
          <input type="phone" class="form-control" name="Phone" required />
        </div>
        <div class="form-group my-5">
          <a href="#" role="Send" class="but but-black py-2 px-4 my-5">Отправить</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные <sup>*</sup>, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на получение от Jaguar Land Rover новостей, а также информации по специальным предложениям и рекламным акциям.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal bg-gray2" data-remodal-id="question">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Задать вопрос</h3></div>
  </div>
  <form data-event="feedback">
    <input type="hidden" name="Form" value="Задать вопрос" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <label class="col-form-label">Ваше имя: <sup>*</sup></label>
          <input type="text" class="form-control" name="Name" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Контактный телефон: <sup>*</sup></label>
          <input type="phone" class="form-control" name="Phone" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Ваш вопрос:</label>
          <textarea class="form-control" rows="5" name="Comment"></textarea>
        </div>
        <div class="form-group my-5">
          <a href="#" role="Send" class="but but-black py-2 px-4 my-5">Отправить</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные <sup>*</sup>, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на получение от Jaguar Land Rover новостей, а также информации по специальным предложениям и рекламным акциям.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal bg-gray2" data-remodal-id="offer">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Получить предложение на Land Rover</h3><h2 role="Car"></h2></div>
  </div>
  <form data-event="special">
    <input type="hidden" name="Form" value="Получить предложение на Land Rover" />
    <input type="hidden" name="Car" value="" />
    <div class="row text-left">
      <div class="col-md-12">
        <div class="form-group">
          <label class="col-form-label">Ваше имя: <sup>*</sup></label>
          <input type="text" class="form-control" name="Name" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Контактный телефон: <sup>*</sup></label>
          <input type="phone" class="form-control" name="Phone" required />
        </div>
        <div class="form-group my-5">
          <a href="#" role="Send" class="but but-black py-2 px-4 my-5">Отправить</a>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные <sup>*</sup>, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на получение от Jaguar Land Rover новостей, а также информации по специальным предложениям и рекламным акциям.</small></p>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal col2 bg-gray2" data-remodal-id="tradein">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Заявка на трейд-ин</h3></div>
  </div>
  <form data-event="sell-car">
    <input type="hidden" name="Form" value="Заявка на трейд-ин" />
    <div class="row text-left">
      <div class="col-md-6">
        <div class="form-group">
          <label class="col-form-label">Модель вашего автомобиля: <sup>*</sup></label>
          <input type="text" class="form-control" name="Model" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Год выпуска: <sup>*</sup></label>
          <input type="number" class="form-control" name="Year" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Двигатель: <sup>*</sup></label>
          <input type="text" class="form-control" name="Engine" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Коробка передач: <sup>*</sup></label>
          <input type="text" class="form-control" name="Transmission" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Пробег: <sup>*</sup></label>
          <input type="number" class="form-control" name="Milleage" required />
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label class="col-form-label">Ваше имя: <sup>*</sup></label>
          <input type="text" class="form-control" name="Name" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Контактный телефон: <sup>*</sup></label>
          <input type="phone" class="form-control" name="Phone" required />
        </div>
        <div class="form-group">
          <label class="col-form-label">Комментарий:</label>
          <textarea class="form-control" name="Comment"></textarea>
        </div>
        <div class="form-group">
          <p><small>Поля, отмеченные <sup>*</sup>, обязательны для заполнения.<br />Сообщая данные сведения, я даю согласие на получение от Jaguar Land Rover новостей, а также информации по специальным предложениям и рекламным акциям.</small></p>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group my-5">
          <a href="#" role="Send" class="but but-black py-2 px-4 my-5">Отправить</a>
        </div>
      </div>
    </div>
  </form>
  <div class="alert alert-dismissible alert-success">
    <strong>Спасибо за вашу заявку!</strong> Мы свяжемся с Вами в ближайшее время.
  </div>
  <div class="alert alert-dismissible alert-danger">
    <strong>Ой, что-то пошло не так!</strong> Повторите попытку позднее.
  </div>
</div>

<div class="remodal col2 bg-gray2" data-remodal-id="disclRangeRover">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5">
      <h3>Подробные условия действия предложения на Range Rover</h3>
      <p><sup>*</sup> Предложение действует с 01.10.2019 по 10.12.2019 года. Предложение ограничено. Автомобиль на изображении может отличаться от представленных в салоне. Полные условия по телефону и в салоне официального дилера Land Rover Юг-Авто. Вся информация носит справочный характер и не является публичной офертой (ст. 437 ГК РФ).</p>
    </div>
  </div>
  <p></p>
</div>
<div class="remodal col2 bg-gray2" data-remodal-id="disclRangeRoverSport">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Подробные условия действия предложения на Range Rover Sport</h3><p><sup>*</sup> Предложение действует с 02.12.2019 по 10.01.2020 года. Cтоимость включает НДС 20%, указана для нового Range Rover Sport с ПТС 2019 года в комплектации SE (Эс Е) с дизельным двигателем объемом 3.0 л. и рассчитана с учетом скидки 200 000 рублей на покупку нового автомобиля по программе Trade In (Трейд ин – обмен автомобиля Jaguar или Land Rover не старше 9 лет, приобретенного клиентом у официального дилера на территории РФ на новый Range Rover Sport в наличии с доплатой в салоне официального дилера Land Rover).</p></div>
  </div>
  <p></p>
</div>
<div class="remodal col2 bg-gray2" data-remodal-id="disclRangeRoverVelar">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Подробные условия действия предложения на Range Rover Velar</h3><p><sup>*</sup> Предложение действует с 02.12.2019 по 10.01.2020 года. Стоимость включает НДС 20% и указана для автомобиля Range Rover Velar с ПТС 2019 года в комплектации Base (Базовая) с дизельным двигателем объемом 2.0 л. мощностью 180 л.с. и рассчитана с учетом скидки 200 000 рублей на покупку нового автомобиля по программе Trade In (Трейд ин – обмен автомобиля Jaguar или Land Rover не старше 9 лет, приобретенного клиентом у официального дилера на территории РФ  на новый Range Rover Velar в наличии с доплатой в салоне официального дилера Land Rover).</p></div>
  </div>
  <p></p>
</div>
<div class="remodal col2 bg-gray2" data-remodal-id="disclRangeRoverEvoque">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Подробные условия действия предложения на Range Rover Evoque</h3><p><sup>*</sup> Предложение действует с 01.10.2019 по 10.01.2020 года. Cтоимость включает НДС 20% и указана для автомобиля НОВЫЙ Range Rover Evoque с ПТС 2019 года в комплектации Standard с дизельным двигателем объемом 2.0 л. мощностью 150 л.с. Стоимость рассчитана с учетом скидки 200 000 рублей на покупку нового автомобиля по программе Trade In (Трейд ин – обмен автомобиля Jaguar или Land Rover не старше 9 лет, приобретенного клиентом у официального дилера на территории РФ  на новый Range Rover Evoque в наличии с доплатой в салоне официального дилера Land Rover).</p></div>
  </div>
  <p></p>
</div>
<div class="remodal col2 bg-gray2" data-remodal-id="disclDiscovery">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Подробные условия действия предложения на Discovery</h3><p><sup>*</sup> Предложение действует с 02.12.2019 по 10.01.2020 года. Преимущество указано для нового автомобиля Discovery в комплектации SЕ (Эс Е) с ПТС 2019 года с дизельным двигателем. Предложение ограничено. Автомобиль на изображении может отличаться от представленных в салоне. Полные условия на сайте официального дилера Land Rover. Вся информация носит справочный характер и не является публичной офертой (ст. 437 ГК РФ).</p></div>
  </div>
  <p></p>
</div>
<div class="remodal col2 bg-gray2" data-remodal-id="disclDiscoverySport">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="row text-left">
    <div class="col-md-12 mb-5"><h3>Подробные условия действия предложения на Новый Discovery Sport</h3><p><sup>*</sup> Предложение действует с 07.10.2019 по 10.01.2020 года. Cтоимость включает НДС 20%, указана для автомобиля НОВЫЙ Discovery Sport 2020 модельного года в комплектации Standard (Стандарт) с бензниновым двигателем объемом 2.0 л. мощностью 200 л.с. Стоимость рассчитана с учетом скидки 150 000 рублей на покупку нового автомобиля по программе Trade In (Трейд ин – обмен автомобиля любой марки не старше 7 лет, приобретенного клиентом у официального дилера на территории РФ  на НОВЫЙ Discovery Sport в наличии с доплатой в салоне официального дилера Land Rover.</p></div>
  </div>
  <p></p>
</div>