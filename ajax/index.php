<?php
	//Load Composer's autoloader
	require __DIR__.'/../vendor/autoload.php';
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	if ( $_POST ) {
		
		$mail = new PHPMailer(true);
		
		try {
		
			//Recipients
			$mail->setFrom('landing@genesis.yug-avto.ru', 'Form Sender');
			$mail->addAddress('callcenter@adv.yug-avto.ru', '');
			
///			$mail->addAddress('anton.boreckiy@yug-avto.ru', '');
			
			$mail->CharSet = 'UTF-8';
		
			//Content
			$mail->isHTML(true);
			$mail->Subject = 'Заявка с лэндинга Land Rover. Форма: "'.$_POST['Form'].'"';
			
			$mail->Body    = 'Заполнена форма "'.$_POST['Form'].'"';
			$mail->Body .= '<br /><br />';
			
			if ( $_POST['Name'] ) $mail->Body .= '<strong>Имя</strong>: '. $_POST['Name'].'<br />';
			if ( $_POST['Phone'] ) $mail->Body .= '<strong>Телефон</strong>: '. $_POST['Phone'].'<br />';
			if ( $_POST['Email'] ) $mail->Body .= '<strong>Email</strong>: '. $_POST['Email'].'<br />';
			if ( $_POST['Date'] ) $mail->Body .= '<strong>Дата</strong>: '. $_POST['Date'].'<br />';
			
			$mail->Body .= '<br /><br />';
			
			if ( $_POST['Car'] ) $mail->Body .= '<strong>Автомобиль</strong>: '. $_POST['Car'].'<br />';
			if ( $_POST['Model'] ) $mail->Body .= '<strong>Автомобиль</strong>: '. $_POST['Model'].'<br />';
			if ( $_POST['Year'] ) $mail->Body .= '<strong>Год выпуска</strong>: '. $_POST['Year'].'<br />';
			if ( $_POST['Engine'] ) $mail->Body .= '<strong>Двигатель</strong>: '. $_POST['Engine'].'<br />';
			if ( $_POST['Transmission'] ) $mail->Body .= '<strong>Коробка передач</strong>: '. $_POST['Transmission'].'<br />';
			if ( $_POST['Milleage'] ) $mail->Body .= '<strong>Пробег</strong>: '. $_POST['Milleage'].'<br />';
			
			$mail->Body .= '<br /><br />';
			if ( $_POST['Comment'] ) $mail->Body .= '<strong>Комментарий</strong>:<br />'. $_POST['Comment'].'<br />';
		
			$mail->send();
			echo json_encode(['status'=>'success']);
		
		} catch (Exception $e) {
			
			echo json_encode(['status'=>'error']);
		}
	}

?>